<?php
/**
 *
 *
 * @author Ryan Spaeth <rspaeth@mvqn.net>
 */

return
[

    "site" =>
    [
        "demo" =>               true,

        "name" =>               "Client Lead Generator",

                                // The return URL to display to the customer after submission.
        "url" =>                "http://localhost/s1/",

        "lang" =>               "en-US"
    ],


    "ucrm" =>                   // UCRM: This section MUST be completed for the forms to work!
    [
        "host" =>               getenv("UCRM_HOST") ?:
                                // Configure your host url here, as it appears while logged into the UCRM system.
                                "http://terraping.com/crm/"
        ,
        "rest" =>
        [
            "url" =>            getenv("UCRM_REST_URL") ?:
                                // The REST Client does not currently support HTTPS for the API calls.
                                "http://terraping.com/crm/api/v1.0"
            ,
            "key" =>            getenv("UCRM_REST_KEY") ?:
                                // Be sure the App Key you generate in the UCRM is of type "Write" or the Client Lead
                                // generation will fail!
                                "FF7cgFHM028SW08j1iTxAOv59j9lY2Ejx9RBcxoFoltjhOJ2ana3CsyaaEmfT8nb"
            ,
        ],
    ],

    "maps" =>                   // MAPS: This section MUST be completed for the forms to work!
    [
        "google" =>             // Only Google Maps is currently supported!
        [
            "api" =>
            [
                "key" =>        getenv("GOOGLE_MAPS_API_KEY") ?:
                                // You should be able use the same key that you are currently using in UCRM or UNMS.
                                "AIzaSyD24-sUYd8QLi6kdRUvX7myezG3a6c8U-Q"
                ,
            ],

            "defaults" =>
            [
                "latitude" =>   (float)(getenv("GOOGLE_MAPS_DEFAULT_LATITUDE") ?:
                                // Set a default Latitude to center on a specific area, if desired.
                                37.20477
                ),

                "longitude" =>  (float)(getenv("GOOGLE_MAPS_DEFAULT_LONGITUDE") ?:
                                // Set a default Longitude to center on a specific area, if desired.
                                -107.79134
                ),

                "zoom" =>       (int)(getenv("GOOGLE_MAPS_DEFAULT_ZOOM") ?:
                                // Set a default Zoom level, if desired.  The higher the number, the closer the view.
                                3
                ),
            ],

            "layers" =>         // A list of KML/KMZ files that should be drawn on the map as overlays...
            [
                                //"http://ucrm.dev.mvqn.net/Meteorite_Impacts_from_around_the_World.kmz",
                                //"http://ucrm.dev.mvqn.net/Wal-Mart_sites.kml"
                                //"http://ucrm.dev.mvqn.net/McDonald's_Europe.kml",
                                //"http://ucrm.dev.mvqn.net/costco-gas-stations.kml",
                                //"http://api.flickr.com/services/feeds/geo/?g=322338@N20&lang=en-us&format=feed-georss"
                                "http://terraping.com/signup/Coverage_only.KMZ"
            ],

            "heatmaps" =>       // Not yet implemented!
            [

            ]
        ]
    ],

];
