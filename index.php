<?php
/**
 * Displays our Client Form as a full page.
 *
 * @author Ryan Spaeth <rspaeth@mvqn.net>
 */
if(file_exists('common.php'))
require_once 'common.php';
if(file_exists("/twig/translations/$language/home.php"))
$translations = include_once "/twig/translations/$language/home.php";

// Render the appropriate template!
echo $twig->render("home.html.twig",
    [
        "config" => $config,
        "language" => $language,
        "translations" => $translations,
        "query" => $query,
        "cookies" => $_COOKIE
    ]);